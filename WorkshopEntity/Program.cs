﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkshopEntity
{
    class Program
    {
        static void Main(string[] args)
        {
            //select();
            //insert();
            //update();
            delete();


        }

        //ข้อ 1
       /* static private void select()
        {
            using (var db = new WorkshopEntityEntities())
            {
                var ds = (from c in db.CUSTOMER
                          join d in db.COUNTRY on c.COUNTRY_CODE equals d.COUNTRY_CODE
                          where c.COUNTRY_CODE == "US"
                          select new {
                              CusID = c.CUSTOMER_ID,
                              CusName = c.NAME,
                              CusEmail = c.EMAIL,
                              ConName = d.COUNTRY_NAME,
                              Cusbudget = c.BUDGET,
                              Cusused = c.USED
                          
                          
                          
                          }).ToList();

                if(ds.Count() > 0)
                {
                    foreach (var item in ds)
                    {
                        Console.WriteLine(string.Format("{0}|{1}|{2}|{3}|{4}|{5}"
                            
                            , item.CusID
                            , item.CusName
                            , item.CusEmail
                            , item.ConName
                            , item.Cusbudget
                            , item.Cusused
                           
                            ));
                    }
                }
            }
            Console.ReadKey();
        }*/



        //ข้อ 2
       /* static private void insert()
        {
            using (var db = new WorkshopEntityEntities())
            {
                db.CUSTOMER.Add(new CUSTOMER() { 
                    CUSTOMER_ID = "C007",
                    NAME = "Satida Bunnakho",
                    EMAIL = "satida15@gmail.com",
                    COUNTRY_CODE = "TH",
                    BUDGET = 10000,
                    USED = 0,
                    

                    });
                db.SaveChanges();
            }

        }*/



        //ข้อ 3

        /*static private void update()
        {
            using (var db = new WorkshopEntityEntities())
            {
                string strName = "J";

                var update = (from c in db.CUSTOMER
                              where c.NAME.Contains(strName)
                              select c).FirstOrDefault();

                if (update != null)
                {
                    update.BUDGET = 0;
                    update.USED = 1;
                }
                db.SaveChanges();
            }

        }*/



        //ข้อ 4



        static private void delete()
        {
            using (var db = new WorkshopEntityEntities())
            {
                string strCustomerId = "C007";
                var ds = db.CUSTOMER.Where(d => d.CUSTOMER_ID == strCustomerId).FirstOrDefault();
                if (db != null)
                {
                    db.CUSTOMER.Remove(ds);
                }
                db.SaveChanges();
            }
        
        }


    }
}
